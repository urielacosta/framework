import React from 'react';
import { BaseStyles, Header } from '@primer/components'
import './style.css';

const HeaderF = () => {
  return (
    <div>
      <BaseStyles>
        <Header className="header">
        <Header.Item>
            <div className="title">Framework</div>
          </Header.Item>
        <Header.Item>
            <Header.Link href="/">Home</Header.Link>
          </Header.Item>
          <Header.Item>
            <Header.Link href="/albuns">Albuns</Header.Link>
          </Header.Item>
          <Header.Item>
            <Header.Link href="/postagens">Postagens</Header.Link>
          </Header.Item>
          <Header.Item>
            <Header.Link href="/todos">Todos</Header.Link>
          </Header.Item>
        </Header>
      </BaseStyles>
    </div>
  );
}

export default HeaderF;