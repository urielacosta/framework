import React from "react";
import { Route, BrowserRouter } from "react-router-dom";

import Home from "../pages/home";
import Albuns from "../pages/albuns";
import Postagens from "../pages/postagens";
import Todos from "../pages/todos";

const Routes = () => {
   return(
       <BrowserRouter>
           <Route component = { Home }  path="/" exact />
           <Route component = { Albuns }  path="/albuns" exact />
           <Route component = { Postagens }  path="/postagens" exact />
           <Route component = { Todos }  path="/todos" exact />
       </BrowserRouter>
   )
}

export default Routes;