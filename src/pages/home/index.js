import React from 'react';
import HeaderF from '../../components/header/headerf'
import { Link } from "react-router-dom";
import './style.css';

const Home = () => {
  return (
    <div>
      <HeaderF />
      <h2>Desafio React</h2>
      <div className="main-home">
        <Link to={'/albuns'}>
          Albuns
      </Link>
        <Link to={'/postagens'}>
          Postagens
        </Link>
        <Link to={'/todos'}>
          Todos
        </Link>
      </div>
    </div>
  );
}

export default Home;