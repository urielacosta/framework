import React from 'react';
import { Box } from '@primer/components'
import HeaderF from '../../components/header/headerf'
import './style.css';
import axios from "axios";

const baseURL = "https://jsonplaceholder.typicode.com/posts/";

const Postagens = () => {
  
  const [post, setPost] = React.useState([]);
  React.useEffect(() => {
    axios.get(baseURL).then((response) => {
      setPost(response.data);
    });
  }, []);

  return (
    <div>
      <HeaderF />
      <h1>Postagens</h1>
      <div className="main-post">
        {
          post.map((each) => (
            <Box borderColor="border" borderWidth={1} borderStyle="solid" p={3}>
              <h3>{each.title}</h3>
              <p>{each.body}</p>
            </Box>
          ))
        }
      </div>
    </div>
  );
}

export default Postagens;