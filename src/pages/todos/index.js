import React from 'react';
import { Box, CircleOcticon } from '@primer/components'
import { XCircleFillIcon, CheckCircleFillIcon } from '@primer/octicons-react'
import HeaderF from '../../components/header/headerf'
import './style.css';
import axios from "axios";

const baseURL = "https://jsonplaceholder.typicode.com/todos/";

const Todos = () => {

  const [todos, setTodos] = React.useState([]);
  React.useEffect(() => {
    axios.get(baseURL).then((response) => {
      setTodos(response.data);
    });
  }, []);


  return (
    <div>
      <HeaderF />
      <h1>Todos</h1>
      <div className="main-todos">
        {
          todos.map((each) => (
            <Box borderColor="border" className="box" borderWidth={1} borderStyle="solid" p={3}>
              <h2>{each.id}</h2>
              <p>{each.title}</p>
              <div className="icons">
                {each.completed ?
                  <CircleOcticon icon={CheckCircleFillIcon} size={32} sx={{ bg: 'icon.success', color: 'text.inverse' }} />
                  :
                  <CircleOcticon icon={XCircleFillIcon} size={32} sx={{ bg: 'icon.success', color: 'text.inverse' }} />
                }
              </div>
            </Box>
          ))
        }
      </div>
    </div>
  );
}

export default Todos;