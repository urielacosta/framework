import React from 'react';
import { Box } from '@primer/components'
import HeaderF from '../../components/header/headerf'
import './style.css';
import axios from "axios";

const baseURL = "https://jsonplaceholder.typicode.com/albums/";

const Home = () => {

  const [albums, setAlbums] = React.useState([]);
  React.useEffect(() => {
    axios.get(baseURL).then((response) => {
      setAlbums(response.data);
    });
  }, []);


  return (
    <div>
      <HeaderF />
      <h1>Albuns</h1>
      <div className="main">
        {
          albums.map((each) => (
            <Box borderColor="border" borderWidth={1} borderStyle="solid" p={3}>
              <h2>{each.id}</h2>
              <p>{each.title}</p>
            </Box>
          ))
        }
      </div>
    </div>
  );
}

export default Home;