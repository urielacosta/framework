import React from 'react';
import Routes from "./routes/routes";
import {ThemeProvider} from '@primer/components'

export default function App() {
   return (
    <ThemeProvider>
       <Routes/>
       </ThemeProvider>
   );
}