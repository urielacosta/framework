FROM node:alpine

WORKDIR /Users/urielcosta/Projects/

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]