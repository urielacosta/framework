# REACT, JSONPLACEHOLDER FAKE API

## Tool Stack

1. React
3. JsonPlaceholder Fake API

## Project Setup

Open your terminal and run the following commands to get your copy of the projct running on your local or remote environment

##### `git clone https://urielacosta@bitbucket.org/urielacosta/framework.git`

Gets a copy of the codebase to your development environment

##### `npm install`

Installs Dependencies and Dev-dependencies required to run the project successfully

After which you can run either of the available scripts that fit your purpose

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
Edit the files as you see fit, if you end up with something you cannot resolve and wish to go back to the original code. Run
`git restore`.<br />
You will also see any lint errors in the console.

### `npm build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready for production & to be deployed!

You can copy the build folder content to your server to host your copy of the project
